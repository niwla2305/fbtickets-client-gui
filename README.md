# fbtickets-client-gui

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn tauri:serve
```

### Compiles and minifies for production
```
yarn tauri:build
```

### Generate icons
```
yarn tauri icon
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/) for vue and [Tauri Docs](https://tauri.studio) for Tauri.
