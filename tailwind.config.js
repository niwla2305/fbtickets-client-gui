module.exports = {
  purge: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        'background': '#0a151f',
        'primary': '#1E5F00',
        'secondary': '#5DBC30'
      },
      dropShadow: {
        'button': '15px 15px 0 rgba(35, 111, 0, 0.47)',
      },
      boxShadow: {
        '8xl': '0 20px 90px 60px rgba(35, 111, 0, 0.2)'
      }
    },
  },
  variants: {
    extend: {
      dropShadow: ['hover', 'focus']
    },
  },
  plugins: [],
}
