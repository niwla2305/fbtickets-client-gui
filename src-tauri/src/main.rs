#![cfg_attr(
all(not(debug_assertions), target_os = "windows"),
windows_subsystem = "windows"
)]

extern crate reqwest;

use reqwest::Response;
use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
struct AppConfig {
    client_name: String,
    client_token: String,
    server_url: String,
    box_url: String
}

impl ::std::default::Default for AppConfig {
    fn default() -> Self { Self { client_name: "".into(), client_token: "".into(), server_url: "".into(), box_url: "http://192.168.178.1".into() } }
}

#[derive(Deserialize)]
struct TicketServerResponse {
    ticket: Option<String>,
    remaining_tickets: Option<i64>,
    error: Option<String>,
}

#[derive(Deserialize, Serialize)]
struct ApplyTicketResponse {
    success: bool,
    remaining_tickets: Option<i64>,
    error: Option<String>,
}

#[derive(Deserialize, Serialize)]
struct TicketCountServerResponse {
    count: i64,
}

#[tauri::command]
async fn get_remaining_tickets() -> i64 {
    let cfg: AppConfig = confy::load("fbtickets").unwrap(); // read config from default location
    confy::store("fbtickets", &cfg).unwrap(); // store config so user can edit empty config file

    // create http client
    let client = reqwest::ClientBuilder::new()
        .build()
        .unwrap();

    // get available ticket count from server
    let count_response = client.get(format!("{}/ticket/{}/count?token={}", &cfg.server_url, &cfg.client_name, &cfg.client_token))
        .send()
        .await.unwrap()
        .json::<TicketCountServerResponse>()
        .await.unwrap();
    return count_response.count;
}


#[tauri::command]
async fn apply_ticket() -> ApplyTicketResponse {
    let cfg: AppConfig = confy::load("fbtickets").unwrap(); // read config from default location
    confy::store("fbtickets", &cfg).unwrap(); // store config so user can edit empty config file

    // create http client
    let client = reqwest::ClientBuilder::new()
        .redirect(reqwest::redirect::Policy::none())
        .build()
        .unwrap();

    // ask server for ticket
    let ticket_response = client.get(format!("{}/ticket/{}?token={}", &cfg.server_url, &cfg.client_name, &cfg.client_token))
        .send()
        .await.unwrap()
        .json::<TicketServerResponse>()
        .await.unwrap();

    return match ticket_response.ticket {

        // if ticket field does not exist something has gone wrong.
        None => {
            ApplyTicketResponse { success: false, remaining_tickets: None, error: ticket_response.error }
        }
        Some(_) => {
            let ticket = ticket_response.ticket.unwrap();
            let remaining_tickets = ticket_response.remaining_tickets;
            println!("{}", ticket);

            let params = [("account", "nil"), ("ticket", &ticket), ("edit", "")]; // change this
            let response: Response = client.post(format!("{}/tools/kids_not_allowed.lua", &cfg.box_url))
                .form(&params)
                .send()
                .await
                .unwrap();
            let text = response.text().await.unwrap();
            let mut apply_ticket_resonse = ApplyTicketResponse {
                success: false,
                remaining_tickets,
                error: None
            };
            if text.contains("erfolgreich") {
                apply_ticket_resonse.success = true;
            } else {
                apply_ticket_resonse.error = Option::from("the box did not accept the ticket".to_string())
            }
            return apply_ticket_resonse
        }
    };
}

fn main() {
    tauri::Builder::default()
        .invoke_handler(tauri::generate_handler![apply_ticket, get_remaining_tickets])
        .run(tauri::generate_context!())
        .expect("error while running fbtickets application");
}
